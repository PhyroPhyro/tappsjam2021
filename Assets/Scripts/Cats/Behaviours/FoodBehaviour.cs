using UnityEngine;

[CreateAssetMenu (fileName = "FoodBehaviour", menuName = "Behaviours/FoodBehaviour")]
public class FoodBehaviour : CatBehaviour
{
    public override ResourceTray GetBehaviourTray()
    {
         return GameServices.GetService<IResourcesManager>().GetAvailableFoodTray();
    }
}
