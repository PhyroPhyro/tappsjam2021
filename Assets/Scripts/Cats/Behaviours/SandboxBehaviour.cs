using UnityEngine;

[CreateAssetMenu (fileName = "SandboxBehaviour", menuName = "Behaviours/SandboxBehaviour")]
public class SandboxBehaviour : CatBehaviour
{
    public override ResourceTray GetBehaviourTray()
    {
         return GameServices.GetService<IResourcesManager>().GetAvailableSandboxTray();
    }
}
