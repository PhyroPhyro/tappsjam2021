using UnityEngine;

[CreateAssetMenu (fileName = "SofaBehaviour", menuName = "Behaviours/SofaBehaviour")]
public class SofaBehaviour : CatBehaviour
{
    public override ResourceTray GetBehaviourTray()
    {
         return GameServices.GetService<IResourcesManager>().GetAvailableSofaTray();
    }
}
