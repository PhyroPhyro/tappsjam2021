using System.Collections;
using UnityEngine;

public class Cat
{
    public bool IsBusy;
    public ResourceTray Tray;

    public IEnumerator ActivityRoutine(ResourceTray tray, float delay)
    {
        yield return new WaitForSeconds(delay);
        Tray = null;
        IsBusy = false;
        GameServices.GetService<IResourcesManager>().UnuseTray(tray);
    }
}
