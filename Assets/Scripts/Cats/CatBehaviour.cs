using UnityEngine;

public class CatBehaviour : ScriptableObject
{
    [Header("Status Modifier")]
    public float SatietyValueModifier, HygieneValueModifier, FunValueModifier;

    [Header("Properties")]
    public float TimeToComplete;
    public string BehaviourAnimation;

    public virtual ResourceTray GetBehaviourTray(){ return null; }
}
