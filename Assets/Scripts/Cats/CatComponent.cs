using System.Collections;
using Character;
using UnityEngine;
using Spine.Unity;

public class CatComponent : InteractableItem
{
    public Cat Cat;
    public SkeletonGraphic CatSpine;
    public MovableCharacter MovableCharacter;
    private Coroutine wanderRoutine;

    void Start()
    {
        CatSpine.Skeleton.SetSkin("Cat" + Random.Range(1,7));
        StartWandering();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            GameServices.GetService<ICatsController>().InterruptCatActivity(Cat);
        }

        if(Input.GetKeyDown(KeyCode.S))
        {
            GameServices.GetService<ICatsController>().UninterruptCatActivity(Cat);
        }
    }

    public override void Interact(PlayerCharacterGraphics player)
    {
        player.HoldCat(this);
    }

    public void StartWandering()
    {
        wanderRoutine = StartCoroutine(WanderRoutine());
    }

    public void PauseWandering()
    {
        SetCatAnimation("idle");
        if(wanderRoutine != null)
            StopCoroutine(wanderRoutine);
    }

    public void SetCatAnimation(string animName)
    {
        CatSpine.AnimationState.SetAnimation(0, animName, true);
    }

    IEnumerator WanderRoutine()
    {
        yield return new WaitForSeconds(Random.Range(0, GameServices.GetService<Configs>().WanderRoutineDelay));
        if(!Cat.IsBusy)
        {
            SetCatAnimation("walk");
            Vector2 randomPositionOnScreen = Configs.GetRandomPositionWithinPlayArea();
            yield return MovableCharacter.MoveTo(randomPositionOnScreen);
            SetCatAnimation("idle");
        }
        wanderRoutine = StartCoroutine(WanderRoutine());
    }
}
