using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CatsController : ICatsController
{
    public event Action<int> OnCatAmountChange;
    public Dictionary<Cat, CatComponent> catList = new Dictionary<Cat, CatComponent>();
    public Dictionary<Cat, Coroutine> catBehaviourRoutines = new Dictionary<Cat, Coroutine>();

    private List<CatBehaviour> catsBehaviours => GameServices.GetService<Configs>().CatBehaviours;
    private Coroutine catBehavioursTimer;
    private Coroutine catSpawnTimer;
    private Coroutine catMoveTimer;

    public int CatAmount => catList.Count;

    public void Setup()
    {
        SpawnInitialCats();
        StartCatsBehaviours();
        StartCatsSpawnTimer();
        GameServices.GetService<IEnergyManager>().OnEnergyChange += LogTestsEnergy;
    }

    public CatComponent GetCat(Cat cat)
    {
        return catList[cat];
    }

    public void AddCat()
    {
        var cat = GameObject.Instantiate(GameServices.GetService<Configs>().CatPrefab, GameObject.FindGameObjectWithTag("Container").transform);
        Vector2 randomPositionOnScreen = Configs.GetRandomPositionWithinPlayArea();
        cat.transform.localPosition = randomPositionOnScreen;
        cat.Cat = new Cat();
        catList.Add(cat.Cat, cat);
        
        OnCatAmountChange?.Invoke(catList.Count);
    }

    public void InterruptCatActivity(Cat cat)
    {
        cat.IsBusy = true;
        GameServices.GetService<IResourcesManager>().UnuseTray(cat.Tray);
        catList[cat].PauseWandering();
        if(catBehaviourRoutines.ContainsKey(cat))
            CoroutineManager.Instance.StopCoroutine(catBehaviourRoutines[cat]);
    }

    public void UninterruptCatActivity(Cat cat)
    {
        cat.IsBusy = false;
        catList[cat].StartWandering();
    }

    public void ForceCatBehaviour(Cat cat, ResourceTray tray, CatBehaviour behaviour)
    {
        GameServices.GetService<IEnergyManager>().AddEnergy(EnergyType.Satiety, behaviour.SatietyValueModifier);
        GameServices.GetService<IEnergyManager>().AddEnergy(EnergyType.Fun, behaviour.FunValueModifier);
        GameServices.GetService<IEnergyManager>().AddEnergy(EnergyType.Hygiene, behaviour.HygieneValueModifier);
        CoroutineManager.Instance.StartCoroutine(MoveCatByBehaviourRoutine(cat, tray, behaviour));
    }

    private void SpawnInitialCats()
    {
        for (int i = 0; i < GameServices.GetService<Configs>().InitialCatsQuantity; i++)
        {
            AddCat();
        }
    }

    private void StartCatsSpawnTimer()
    {
        if(catSpawnTimer != null)
            CoroutineManager.Instance.StopCoroutine(catSpawnTimer);
        
        catSpawnTimer = CoroutineManager.Instance.StartCoroutine(CatsSpawnTimer(GetSpawnDelay()));
    }

    private void StartCatsBehaviours()
    {
        if(catBehavioursTimer != null)
            CoroutineManager.Instance.StopCoroutine(catBehavioursTimer);
        
        catBehavioursTimer = CoroutineManager.Instance.StartCoroutine(CatsBehaviourTimer(GetBehaviourDelay()));
    }

    private float GetBehaviourDelay()
    {
        var config = GameServices.GetService<Configs>();
        return Random.Range(config.MinCatsBehaviourTimerDelay, config.MaxCatsBehaviourTimerDelay); 
    }

    private float GetSpawnDelay()
    {
        var config = GameServices.GetService<Configs>();
        return Random.Range(config.MinCatsSpawnTimerDelay, config.MaxCatsSpawnTimerDelay); 
    }

    IEnumerator CatsBehaviourTimer(float delay)
    {
        yield return new WaitForSeconds(delay);
        ExecuteNextBehaviour();
        catBehavioursTimer = CoroutineManager.Instance.StartCoroutine(CatsBehaviourTimer(GetBehaviourDelay()));
    }

    IEnumerator CatsSpawnTimer(float delay)
    {
        yield return new WaitForSeconds(delay);
        ExecuteSpawn();
        catBehavioursTimer = CoroutineManager.Instance.StartCoroutine(CatsSpawnTimer(GetSpawnDelay()));
    }

    private void ExecuteNextBehaviour()
    {
        var nextCat = GetAvailableCat();
        if(nextCat != null)
        {
            var availableBehaviours = catsBehaviours.FindAll(b => b.GetBehaviourTray() != null);
            if(availableBehaviours.Count > 0)
            {
                var energyManager = GameServices.GetService<IEnergyManager>();
                var currentBehaviour = availableBehaviours[Random.Range(0, availableBehaviours.Count)];
                energyManager.AddEnergy(EnergyType.Satiety, currentBehaviour.SatietyValueModifier);
                energyManager.AddEnergy(EnergyType.Hygiene, currentBehaviour.HygieneValueModifier);
                energyManager.AddEnergy(EnergyType.Fun, currentBehaviour.FunValueModifier);

                var resourcesManager = GameServices.GetService<IResourcesManager>();
                var tray = currentBehaviour.GetBehaviourTray();
                resourcesManager.UseTray(currentBehaviour.GetBehaviourTray(), nextCat);

                if(catBehaviourRoutines.ContainsKey(nextCat))
                    catBehaviourRoutines[nextCat] = CoroutineManager.Instance.StartCoroutine(MoveCatByBehaviourRoutine(nextCat, tray, currentBehaviour));
                else
                    catBehaviourRoutines.Add(nextCat, CoroutineManager.Instance.StartCoroutine(MoveCatByBehaviourRoutine(nextCat, tray, currentBehaviour)));
            }else{
                Debug.Log("No activities available.");
            }
        }else{
            Debug.Log("No cats available.");
        }
    }

    private void ExecuteSpawn()
    {
        AddCat();
        Debug.Log($"Spawning new cats. Current cats:{catList.Count}");
        //TODO - Visuals
    }

    private void LogTestsEnergy(EnergyType type, float qnty)
    {
        // Debug.Log($"{type} {qnty}");
    }

    private Cat GetAvailableCat()
    {
        var availableCats = new List<Cat>();

        foreach (var item in catList)
        {
            if(!item.Key.IsBusy)
                availableCats.Add(item.Key);
        }

        if(availableCats.Count > 0)
            return availableCats[Random.Range(0, availableCats.Count)];

        return null;
    }

    IEnumerator MoveCatByBehaviourRoutine(Cat cat, ResourceTray tray, CatBehaviour behaviour)
    {
        catList[cat].PauseWandering();

        cat.IsBusy = true;
        cat.Tray = tray;
        catList[cat].SetCatAnimation("walk");
        yield return catList[cat].MovableCharacter.MoveTo(GameServices.GetService<IResourcesManager>().GetTrayComponent(tray).gameObject);
        catList[cat].SetCatAnimation(behaviour.BehaviourAnimation);
        yield return cat.ActivityRoutine(tray, behaviour.TimeToComplete);

        catList[cat].StartWandering();
    }
}
