using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICatsController
{
    event Action<int> OnCatAmountChange;
    int CatAmount { get; }
    void Setup();
    CatComponent GetCat(Cat cat);
    void AddCat();
    void InterruptCatActivity(Cat cat);
    void UninterruptCatActivity(Cat cat);
    void ForceCatBehaviour(Cat cat, ResourceTray tray, CatBehaviour behaviour);
}
