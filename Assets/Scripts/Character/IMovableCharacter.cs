using System.Collections;
using UnityEngine;

namespace Character
{
    public interface IMovableCharacter
    {
        IEnumerator MoveTo(GameObject targetObject);
        IEnumerator MoveTo(Vector2 targetPosition);
    }
}