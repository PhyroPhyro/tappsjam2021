using System.Collections;
using Character;
using UnityEngine;

public class MovableCharacter : MonoBehaviour, IMovableCharacter
{
    [SerializeField] private float speed = 5;

    public IEnumerator MoveTo(GameObject targetObject)
    {
        yield return MoveTo(targetObject.transform.position);
    }

    public IEnumerator MoveTo(Vector2 targetPosition)
    {
        transform.localScale = transform.position.x < targetPosition.x ? Vector3.one : new Vector3(-1,1,1);
        while (Vector2.Distance(transform.position, targetPosition) > 0.1f)
        {
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
            yield return null;
        }
    }
}
