using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Character
{
    public class PlayerCharacterGraphics : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private MovableCharacter movementController;
        [SerializeField] private PlayerItem heldItem;
        [SerializeField] private GameObject catRoot;
        [SerializeField] private CatComponent heldCat;

        [SerializeField] private GameObject gameRoot;
        [SerializeField] private SkeletonGraphic HumanSpine;

        private Coroutine moveCoroutine;

        private void Awake()
        {
            GameServices.GetService<IItemManager>().OnItemChange += HoldItem;
        }

        private void OnDestroy()
        {
            GameServices.GetService<IItemManager>().OnItemChange -= HoldItem;
        }

        public void SetHumanAnimation(string animName)
        {
            HumanSpine.AnimationState.SetAnimation(0, animName, true);
        }

        public void HoldItem(ItemType item)
        {
            if (heldCat != null)
            {
                ReleaseCat(true);
            }

            heldItem.gameObject.SetActive(item != ItemType.None);
            heldItem.SetItem(item);
        }

        public void HoldCat(CatComponent cat)
        {
            GameServices.GetService<IItemManager>().SetSelectedItem(ItemType.None);

            GameServices.GetService<ICatsController>().InterruptCatActivity(cat.Cat);

            Transform catTransform = cat.transform;
            catTransform.SetParent(catRoot.transform);
            heldCat = cat;
        }

        public Cat ReleaseCat(bool resumeCatActivity)
        {
            if (heldCat == null)
            {
                return null;
            }

            heldCat.transform.SetParent(gameRoot.transform);
            if (resumeCatActivity)
            {
                GameServices.GetService<ICatsController>().UninterruptCatActivity(heldCat.Cat);
            }

            var releasedCat = heldCat;
            heldCat = null;

            return releasedCat.Cat;
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                PointerEventData pointerData = new PointerEventData(EventSystem.current);

                pointerData.position = Input.mousePosition;

                List<RaycastResult> results = new List<RaycastResult>();
                EventSystem.current.RaycastAll(pointerData, results);

                if (results.Count > 0)
                {
                    if (results[0].gameObject.layer == LayerMask.NameToLayer("WorldUI"))
                    {
                        if (moveCoroutine != null)
                        {
                            StopCoroutine(moveCoroutine);
                        }

                        moveCoroutine = StartCoroutine(MoveToObjectAndInteract(results[results.Count - 1].gameObject
                            .GetComponent<InteractableItem>()));
                        results.Clear();
                    }
                }
            }
        }

        private IEnumerator MoveToObjectAndInteract(InteractableItem target)
        {
            Transform playerPoint = target.transform.Find("PlayerPoint");

            SetHumanAnimation("walk");

            if(playerPoint != null)
                yield return movementController.MoveTo(playerPoint.transform.position);
            else
                yield return movementController.MoveTo(target.transform.position);

            SetHumanAnimation("interaction");

            yield return new WaitForSeconds(0.5f);

            SetHumanAnimation("idle");

            target.Interact(this);
        }
    }
}