using UnityEngine;

public class Closet : MonoBehaviour
{
    [SerializeField] private GameObject ClosetUI;
    [SerializeField] private ClosetGraphics closetGraphics;

    private void OnEnable()
    {
        GameServices.GetService<IItemManager>().OnItemChange += ItemChanged;
    }

    private void OnDisable()
    {
        GameServices.GetService<IItemManager>().OnItemChange -= ItemChanged;
    }

    private void ItemChanged(ItemType itemType)
    {
        ClosetUI.SetActive(false);
        closetGraphics.SetSpriteClosed();
    }
}
