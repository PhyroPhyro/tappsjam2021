using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(menuName = "Scriptable-Obj/Config", fileName = "Configs")]
public class Configs : ScriptableObject
{
    [Header("Player Values")]
    [Range(0,100)]
    public float SatietyStartValue, HygieneStartValue, FunStartValue;
    public float SatietyChangePerSecond, HygieneChangePerSecond, FunChangePerSecond;

    public float MaxEnergyValue = 100;
    [FormerlySerializedAs("HygieneChangePerClean")] public float HygieneChangePerSandClean;

    [Header("Cats")]
    public CatComponent CatPrefab;
    public List<CatBehaviour> CatBehaviours;
    public float InitialCatsQuantity;
    public float MinCatsBehaviourTimerDelay, MaxCatsBehaviourTimerDelay;
    public float MinCatsSpawnTimerDelay, MaxCatsSpawnTimerDelay;
    public float WanderRoutineDelay;

    [Header("Resources/Trays")]
    public List<Sprite> FoodTraySpriteStates;
    public List<Sprite> SandBoxSpriteStates;

    [Header("Dust")]
    public DustGraphics DustPrefab;
    public int DustHygieneValue, DustFunValue;
    public int MinDustSpawnDelay, MaxDustSpawnDelay;

    public List<Item> ItemsConfigs;

    public static bool CheckBoundaires(Vector2 position)
    {
        var playArea1 = GameObject.FindGameObjectWithTag("PlayArea1");
        var playArea2 = GameObject.FindGameObjectWithTag("PlayArea2");

        return position.x >= playArea1.transform.position.x && position.x <= playArea2.transform.position.x
                && position.y >= playArea1.transform.position.y && position.y <= playArea2.transform.position.y;
    }

    public static bool CheckDustBoundaires(Vector2 position)
    {
        var playArea1 = GameObject.Find("DustArea1");
        var playArea2 = GameObject.Find("DustArea2");

        return position.x >= playArea1.transform.position.x && position.x <= playArea2.transform.position.x
                && position.y >= playArea1.transform.position.y && position.y <= playArea2.transform.position.y;
    }

    public static Vector2 GetRandomPositionWithinPlayArea()
    {
        Vector2 randomPositionOnScreen = Camera.main.ViewportToWorldPoint(new Vector2(Random.value, Random.value));
            while(!Configs.CheckBoundaires(randomPositionOnScreen))
                randomPositionOnScreen = Camera.main.ViewportToWorldPoint(new Vector2(Random.value, Random.value));

        return randomPositionOnScreen;
    }

    public static Vector2 GetRandomPositionWithinDustArea()
    {
        Vector2 randomPositionOnScreen = Camera.main.ViewportToWorldPoint(new Vector2(Random.value, Random.value));
            while(!Configs.CheckDustBoundaires(randomPositionOnScreen))
                randomPositionOnScreen = Camera.main.ViewportToWorldPoint(new Vector2(Random.value, Random.value));

        return randomPositionOnScreen;
    }
}
