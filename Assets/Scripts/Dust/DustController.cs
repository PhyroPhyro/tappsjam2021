using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DustController
{
    private Coroutine dustSpawnTimer;

    public void StartDustSpawnTimer()
    {
        if(dustSpawnTimer != null)
            CoroutineManager.Instance.StopCoroutine(dustSpawnTimer);
        
        dustSpawnTimer = CoroutineManager.Instance.StartCoroutine(DustBehaviourTimer(GetSpawnDelay()));
    }

    private float GetSpawnDelay()
    {
        var config = GameServices.GetService<Configs>();
        return Random.Range(config.MinCatsSpawnTimerDelay, config.MaxCatsSpawnTimerDelay); 
    }

    IEnumerator DustBehaviourTimer(float delay)
    {
        yield return new WaitForSeconds(delay);
        SpawnDust();
        dustSpawnTimer = CoroutineManager.Instance.StartCoroutine(DustBehaviourTimer(GetSpawnDelay()));
    }

    private void SpawnDust()
    {
        var dust = GameObject.Instantiate(GameServices.GetService<Configs>().DustPrefab, GameObject.Find("DustContainer").transform);
        Vector2 randomPositionOnScreen = Configs.GetRandomPositionWithinDustArea();
        dust.transform.position = randomPositionOnScreen;
    }
}
