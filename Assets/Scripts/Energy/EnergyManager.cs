using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyManager : IEnergyManager
{
    public event Action<EnergyType, float> OnEnergyChange;
    public event Action<EnergyType, bool> OnEnergyLimit;

    private Dictionary<EnergyType, float> energiesDict;
    private Configs configs;
    
    public EnergyManager()
    {
        configs = GameServices.GetService<Configs>();
        energiesDict = new Dictionary<EnergyType, float>
        {
            {EnergyType.Satiety, configs.SatietyStartValue},
            {EnergyType.Hygiene, configs.HygieneStartValue},
            {EnergyType.Fun, configs.FunStartValue},
        };

        CoroutineManager.Instance.StartCoroutine(ChangeEnergiesPerTime(EnergyType.Satiety, configs.SatietyChangePerSecond));
        CoroutineManager.Instance.StartCoroutine(ChangeEnergiesPerTime(EnergyType.Hygiene, configs.HygieneChangePerSecond));
        CoroutineManager.Instance.StartCoroutine(ChangeEnergiesPerTime(EnergyType.Fun, configs.FunChangePerSecond));
    }
    
    public void AddEnergy(EnergyType energyType, float amount)
    {
        energiesDict[energyType] += amount;
        
        OnEnergyChange?.Invoke(energyType, energiesDict[energyType]);

        bool reachedLowerLimit = energiesDict[energyType] < 0;
        bool reachedUpperLimit = energiesDict[energyType] > configs.MaxEnergyValue;
        if (reachedLowerLimit || reachedUpperLimit)
        {
            OnEnergyLimit?.Invoke(energyType, !reachedLowerLimit);
        }
    }

    public void SetEnergy(EnergyType energyType, float amount)
    {
        energiesDict[energyType] = amount;
        
        OnEnergyChange?.Invoke(energyType, energiesDict[energyType]);
    }

    private IEnumerator ChangeEnergiesPerTime(EnergyType energyType, float amount)
    {
        yield return new WaitForSeconds(1f);
        AddEnergy(energyType, amount);
        CoroutineManager.Instance.StartCoroutine(ChangeEnergiesPerTime(energyType, amount));
    }
}
