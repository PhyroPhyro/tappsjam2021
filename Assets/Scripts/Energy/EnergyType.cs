using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnergyType
{
    Satiety = 0,
    Hygiene = 1,
    Fun = 2
}
