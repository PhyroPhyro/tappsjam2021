using System;

public interface IEnergyManager
{
    event Action<EnergyType, float> OnEnergyChange;
    event Action<EnergyType, bool> OnEnergyLimit;
    void AddEnergy(EnergyType energyType, float amount);
    void SetEnergy(EnergyType energyType, float amount);
}
