﻿using System;
using UnityEngine;

public class CoroutineManager : MonoBehaviour
{
    private static CoroutineManager instance;
    private static GameObject obj;

    public static CoroutineManager Instance
    {
        get
        {
            if (instance == null)
            {
                obj = new GameObject(typeof(MonoBehaviour).Name);
                instance = obj.AddComponent<CoroutineManager>();
                DontDestroyOnLoad(obj);
            }

            return instance;
        }
    }

    public static void DestroyCoroutineObject()
    {
        instance = null;
        Destroy(obj);
    }
}