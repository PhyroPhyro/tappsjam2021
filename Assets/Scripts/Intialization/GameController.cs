using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject gameOverObject, pauseObject;
    
    void Start()
    {
        GameServices.GetService<IResourcesManager>().SetupTrays();
        GameServices.GetService<ICatsController>().Setup();
        
        var dustController = new DustController();
        dustController.StartDustSpawnTimer();
        GameServices.RegisterService<DustController>(dustController);
    }

    private void OnEnable()
    {
        GameServices.GetService<IEnergyManager>().OnEnergyLimit += GameOver;
        GameServices.GetService<IPauseManager>().OnPauseChange += OnPause;
    }

    private void OnDisable()
    {
        GameServices.GetService<IEnergyManager>().OnEnergyLimit -= GameOver;
        GameServices.GetService<IPauseManager>().OnPauseChange -= OnPause;
    }

    private void GameOver(EnergyType energyType, bool limit)
    {
        GameServices.GetService<IPauseManager>().PauseGame(true);
        gameOverObject.SetActive(true);
        CoroutineManager.DestroyCoroutineObject();
    }

    private void OnPause(bool pause)
    {
        pauseObject.SetActive(pause);
    }
}
