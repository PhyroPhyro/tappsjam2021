using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameInitialization : MonoBehaviour
{
    public Configs Configs;

    void Start()
    {
        InitializeGameServices();
        StartCoroutine(StartGame());
    }

    private void InitializeGameServices()
    {
        GameServices.Clear();
        
        var catsController = new CatsController();
        GameServices.RegisterService<ICatsController>(catsController);

        GameServices.RegisterService<Configs>(Configs);

        var energiesManager = new EnergyManager();
        GameServices.RegisterService<IEnergyManager>(energiesManager);

        var itemManager = new ItemManager();
        GameServices.RegisterService<IItemManager>(itemManager);

        var resourcesManager = new ResourcesManager();
        GameServices.RegisterService<IResourcesManager>(resourcesManager);

        var pauseManager = new PauseManager();
        GameServices.RegisterService<IPauseManager>(pauseManager);
    }

    private IEnumerator StartGame()
    {
        yield return new WaitForSeconds(2f);
        
        SceneManager.LoadScene("Main");
    }
}
