using Character;
using UnityEngine;
using UnityEngine.UI;

public class ClosetGraphics : InteractableItem
{
    [SerializeField] private GameObject closetContent;
    [SerializeField] private Image closetImage;
    [SerializeField] private Sprite closetOpenedSprite, closetClosedSprite;
    
    public override void Interact(PlayerCharacterGraphics player)
    {
        closetImage.sprite = closetOpenedSprite;
        closetContent.SetActive(true);
    }

    public void SetSpriteClosed()
    {
        closetImage.sprite = closetClosedSprite;
    }
}