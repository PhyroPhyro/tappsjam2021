using System.Collections;
using System.Collections.Generic;
using Character;
using UnityEngine;

public class DustGraphics : InteractableItem
{
    [SerializeField] private Animator animator;
    
    public override void Interact(PlayerCharacterGraphics player)
    {
        if (GameServices.GetService<IItemManager>().SelectedItem == ItemType.VacuumCleaner)
        {
            animator.SetTrigger("clean");
            Configs configs = GameServices.GetService<Configs>();
            GameServices.GetService<IEnergyManager>().AddEnergy(EnergyType.Hygiene, configs.DustHygieneValue);
            GameServices.GetService<IEnergyManager>().AddEnergy(EnergyType.Fun, configs.DustFunValue);
            Destroy(gameObject, 1);
        }
    }
}
