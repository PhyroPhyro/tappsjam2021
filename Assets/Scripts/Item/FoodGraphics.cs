using System;
using Character;

public class FoodGraphics : InteractableItem
{
    private TrayComponent trayComponent;

    private void Awake()
    {
        trayComponent = GetComponent<TrayComponent>();
    }

    public override void Interact(PlayerCharacterGraphics player)
    {
        if (GameServices.GetService<IItemManager>().SelectedItem == ItemType.Food)
        {
            GameServices.GetService<IResourcesManager>().FillTray(trayComponent.ResourceTray);
        }
    }
}