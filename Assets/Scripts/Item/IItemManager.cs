using System;
using UnityEngine;

public interface IItemManager
{
    event Action<ItemType> OnItemChange;
    ItemType SelectedItem { get; }
    void SetSelectedItem(ItemType item);
    Sprite GetSelectedItemSprite(ItemType item);
}
