using Character;
using UnityEngine;

public abstract class InteractableItem : MonoBehaviour
{
    public virtual void Interact(PlayerCharacterGraphics player){}
}