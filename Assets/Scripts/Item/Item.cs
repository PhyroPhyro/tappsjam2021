using System;
using UnityEngine;

[Serializable]
public class Item
{
    public ItemType ItemType;
    public Sprite Sprite;
}
