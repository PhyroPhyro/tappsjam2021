using System;
using UnityEngine;

public class ItemManager : IItemManager
{
    public event Action<ItemType> OnItemChange;
    public ItemType SelectedItem { get; private set; }
    
    public void SetSelectedItem(ItemType item)
    {
        SelectedItem = item == SelectedItem ? ItemType.None : item;
        OnItemChange?.Invoke(SelectedItem);
    }

    public Sprite GetSelectedItemSprite(ItemType item)
    {
        var configs = GameServices.GetService<Configs>();

        foreach (var itemConfig in configs.ItemsConfigs)
        {
            if (itemConfig.ItemType == item)
            {
                return itemConfig.Sprite;
            }
        }

        return null;
    }
}
