public enum ItemType
{
    None = -1,
    Food = 0,
    Sand = 1,
    VacuumCleaner = 2
}
