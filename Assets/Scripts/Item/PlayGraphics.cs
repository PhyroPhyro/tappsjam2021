using Character;
using UnityEngine;

public class PlayGraphics : InteractableItem
{
    [SerializeField] private TrayComponent tray;
    [SerializeField] private CatBehaviour behaviour;

    public override void Interact(PlayerCharacterGraphics player)
    {
        var cat = player.ReleaseCat(false);
        if (cat != null)
        {
            GameServices.GetService<ICatsController>().ForceCatBehaviour(cat, tray.ResourceTray, behaviour);
        }
    }
}