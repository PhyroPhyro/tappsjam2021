using Character;

public class SandboxGraphics : InteractableItem
{
    private TrayComponent trayComponent;

    private void Awake()
    {
        trayComponent = GetComponent<TrayComponent>();
    }
    
    public override void Interact(PlayerCharacterGraphics player)
    {
        if (GameServices.GetService<IItemManager>().SelectedItem == ItemType.Sand)
        {
            GameServices.GetService<IResourcesManager>().FillTray(trayComponent.ResourceTray);
            Configs configs = GameServices.GetService<Configs>();
            GameServices.GetService<IEnergyManager>().AddEnergy(EnergyType.Hygiene, configs.HygieneChangePerSandClean);
        }
    }
}