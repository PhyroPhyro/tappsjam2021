using Character;
using UnityEngine;

public class SofaGraphics : InteractableItem
{
    [SerializeField] private TrayComponent tray;
    
    public override void Interact(PlayerCharacterGraphics player)
    {
        if (tray.ResourceTray.Cat != null)
        {
            var cat = GameServices.GetService<ICatsController>().GetCat(tray.ResourceTray.Cat);
            player.HoldCat(cat);
        }
    }
}