using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPauseManager
{
    event Action<bool> OnPauseChange;
    void PauseGame(bool pause);
}
