using System;
using UnityEngine;

public class PauseManager : IPauseManager
{
    public event Action<bool> OnPauseChange;

    public void PauseGame(bool pause)
    {
        Time.timeScale = pause ? 0 : 1;
        OnPauseChange?.Invoke(pause);
    }
}
