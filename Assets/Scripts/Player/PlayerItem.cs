using UnityEngine;
using UnityEngine.UI;

public class PlayerItem : MonoBehaviour
{
    [SerializeField] private Image itemimage;

    public void SetItem(ItemType itemType)
    {
        itemimage.sprite = GameServices.GetService<IItemManager>().GetSelectedItemSprite(itemType);
    }
}
