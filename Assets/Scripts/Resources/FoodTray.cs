
public class FoodTray : ResourceTray
{
    public FoodTray()
    {
        FillTray();
    }

    public override void FillTray()
    {
        Quantity = GameServices.GetService<Configs>().FoodTraySpriteStates.Count;
    }
}
