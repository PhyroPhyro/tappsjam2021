using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResourcesManager
{
    void SetupTrays();

    FoodTray GetAvailableFoodTray();
    SandboxTray GetAvailableSandboxTray();
    SofaTray GetAvailableSofaTray();

    TrayComponent GetTrayComponent(ResourceTray tra);

    void FillTray(ResourceTray tray);

    void UseTray(ResourceTray tray, Cat cat);
    void UnuseTray(ResourceTray tray);
}
