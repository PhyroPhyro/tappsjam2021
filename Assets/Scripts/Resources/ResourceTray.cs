using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceTray
{
    public float Quantity;
    public bool IsAvailable = true;
    public Cat Cat;

    public virtual void FillTray(){}
}
