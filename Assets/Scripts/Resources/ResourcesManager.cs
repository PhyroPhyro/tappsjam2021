using System.Collections.Generic;
using UnityEngine;

public class ResourcesManager : IResourcesManager
{
    private Dictionary<FoodTray, TrayComponent> foodTrays;
    private Dictionary<SandboxTray, TrayComponent> sandboxTrays;
    private Dictionary<SofaTray, TrayComponent> sofaTrays;
    private Dictionary<PlayTray, TrayComponent> playTrays;

    public void SetupTrays()
    {
        foodTrays = new Dictionary<FoodTray, TrayComponent>();
        var foodTrayObjects = GameObject.FindGameObjectsWithTag("Food");
        for (int i = 0; i < foodTrayObjects.Length; i++)
        {   
            var tray = new FoodTray();
            var trayComponent = foodTrayObjects[i].GetComponent<TrayComponent>();
            trayComponent.ResourceTray = tray;
            foodTrays.Add(tray, trayComponent);
        }

        sandboxTrays = new Dictionary<SandboxTray, TrayComponent>();
        var sandboxTrayObjects = GameObject.FindGameObjectsWithTag("Sandbox");
        for (int i = 0; i < sandboxTrayObjects.Length; i++)
        {
            var tray = new SandboxTray();
            var trayComponent = sandboxTrayObjects[i].GetComponent<TrayComponent>();
            trayComponent.ResourceTray = tray;
            sandboxTrays.Add(tray, trayComponent);
        }

        sofaTrays = new Dictionary<SofaTray, TrayComponent>();
        var sofaTrayObjects = GameObject.FindGameObjectsWithTag("Sofa");
        for (int i = 0; i < sofaTrayObjects.Length; i++)
        {
            var tray = new SofaTray();
            var trayComponent = sofaTrayObjects[i].GetComponent<TrayComponent>();
            trayComponent.ResourceTray = tray;
            sofaTrays.Add(tray, trayComponent);
        }

        playTrays = new Dictionary<PlayTray, TrayComponent>();
        var playTrayObjects = GameObject.FindGameObjectsWithTag("Play");
        for (int i = 0; i < playTrayObjects.Length; i++)
        {
            var tray = new PlayTray();
            var trayComponent = playTrayObjects[i].GetComponent<TrayComponent>();
            trayComponent.ResourceTray = tray;
            playTrays.Add(tray, trayComponent);
        }
    }

    public void UseTray(ResourceTray tray, Cat cat)
    {
        tray.IsAvailable = false;
        if(!(tray is SofaTray))
            tray.Quantity--;
        tray.Cat = cat;

        //Calls visual feedback, maybe?
    }

    public void UnuseTray(ResourceTray tray)
    {
        if(tray == null)
            return;

        tray.IsAvailable = true;
        tray.Cat = null;
        RefreshTrayGraphics(tray);
    }

    public TrayComponent GetTrayComponent(ResourceTray tray)
    {
        if(tray is FoodTray)
        {
            return foodTrays[tray as FoodTray];
        }

        if(tray is SandboxTray)
        {
            return sandboxTrays[tray as SandboxTray];
        }

        if(tray is SofaTray)
        {
            return sofaTrays[tray as SofaTray];
        }

        if(tray is PlayTray)
        {
            return playTrays[tray as PlayTray];
        }

        return null;
    }

    public void FillTray(ResourceTray tray)
    {
        tray.FillTray();
        RefreshTrayGraphics(tray);
    }

    public FoodTray GetAvailableFoodTray()
    {
        foreach (var tray in foodTrays)
        {
            if(tray.Key.Quantity > 0 && tray.Key.IsAvailable)
                return tray.Key;
        }

        return null;
    }

    public SofaTray GetAvailableSofaTray()
    {
        foreach (var tray in sofaTrays)
        {
            if(tray.Key.IsAvailable)
                return tray.Key;
        }

        return null;
    }

    public SandboxTray GetAvailableSandboxTray()
    {
        foreach (var tray in sandboxTrays)
        {
            if(tray.Key.Quantity > 0 && tray.Key.IsAvailable)
                return tray.Key;
        }

        return null;    
    }

    private void RefreshTrayGraphics(ResourceTray tray)
    {
        Configs configs = GameServices.GetService<Configs>();
        int index = (int)tray.Quantity - 1;

        if (index < 0)
        {
            return;
        }
        
        if(tray is FoodTray)
        {
            foodTrays[tray as FoodTray].RefreshGraphics(configs.FoodTraySpriteStates[index]);
        }

        if(tray is SandboxTray)
        {
            sandboxTrays[tray as SandboxTray].RefreshGraphics(configs.SandBoxSpriteStates[index]);
        }
    }
}
