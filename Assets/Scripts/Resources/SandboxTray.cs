
public class SandboxTray : ResourceTray
{
    public SandboxTray()
    {
        FillTray();
    }
    
    public override void FillTray()
    {
        Quantity = GameServices.GetService<Configs>().SandBoxSpriteStates.Count;
    }
}
