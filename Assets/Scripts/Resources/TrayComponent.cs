using UnityEngine;
using UnityEngine.UI;

public class TrayComponent : MonoBehaviour
{
    [SerializeField] private Image trayImage;
    public ResourceTray ResourceTray;

    public void RefreshGraphics(Sprite sprite)
    {
        if (trayImage != null)
        {
            trayImage.sprite = sprite;
        }
    }
}
