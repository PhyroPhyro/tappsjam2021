using System;
using TMPro;
using UnityEngine;

public class CatCounter : MonoBehaviour
{
    [SerializeField] private TMP_Text counterText;

    private void OnEnable()
    {
        GameServices.GetService<ICatsController>().OnCatAmountChange += CatAmountChanged;
        
        CatAmountChanged(GameServices.GetService<ICatsController>().CatAmount);
    }

    private void OnDisable()
    {
        GameServices.GetService<ICatsController>().OnCatAmountChange -= CatAmountChanged;
    }

    private void CatAmountChanged(int amount)
    {
        counterText.text = amount.ToString();
    }
}
