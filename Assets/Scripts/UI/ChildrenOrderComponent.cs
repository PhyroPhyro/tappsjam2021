using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildrenOrderComponent : MonoBehaviour
{
   void Update()
    {
        var tmpCount = transform.childCount;
        for (int i = 0; i < tmpCount; i++)
        {
            for (int j = 0; j < tmpCount; j++)
            {
                if(transform.GetChild(i) != null && transform.GetChild(j) != null)
                {
                    if(transform.GetChild(i).position.y > transform.GetChild(j).position.y)
                        transform.GetChild(i).SetSiblingIndex(j);
                }
            }
        }
    }
}
