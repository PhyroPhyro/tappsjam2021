using UnityEngine;
using UnityEngine.UI;

public class ClosetItem : MonoBehaviour
{
    public ItemType itemType;
    [SerializeField] private Button itemButton;

    private IItemManager itemManager;

    private void Start()
    {
        itemManager = GameServices.GetService<IItemManager>();
        itemButton.onClick.AddListener(SelectItem);
    }

    private void SelectItem()
    {
        itemManager.SetSelectedItem(itemType);
    }
}
