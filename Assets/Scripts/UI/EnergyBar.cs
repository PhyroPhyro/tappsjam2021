using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{
    [SerializeField] private EnergyType energyType;
    [SerializeField] private Image fillImage;

    private Configs configs;
    private Coroutine fillImageCoroutine;
    
    private void Awake()
    {
        configs = GameServices.GetService<Configs>();
    }

    private void OnEnable()
    {
        GameServices.GetService<IEnergyManager>().OnEnergyChange += EnergyChange;
    }

    private void OnDisable()
    {
        GameServices.GetService<IEnergyManager>().OnEnergyChange -= EnergyChange;
    }

    private void EnergyChange(EnergyType energyType, float amount)
    {
        if (this.energyType == energyType)
        {
            if (fillImageCoroutine != null)
            {
                StopCoroutine(fillImageCoroutine);
            }
            
            fillImageCoroutine = StartCoroutine(FillImageCoroutine(amount));
        }
    }

    private IEnumerator FillImageCoroutine(float amount)
    {
        float fraction = 0;
        float initValue = fillImage.fillAmount;
        float finalValue = amount / configs.MaxEnergyValue;

        while (fraction <= 1)
        {
            fillImage.fillAmount = Mathf.Lerp(initValue, finalValue, fraction);
            fraction += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}
