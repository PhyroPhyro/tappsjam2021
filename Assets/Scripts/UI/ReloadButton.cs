using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadButton : MonoBehaviour
{
    public void RestartGame()
    {
        GameServices.GetService<IPauseManager>().PauseGame(false);
        SceneManager.LoadScene("Start");
    }
}
